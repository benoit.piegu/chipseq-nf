 - ChIP-seq_pipeline
Benoît Piégu <benoit.piegu@inrae.fr>; Coralie Gimonnet

:toc: left
:numbered: 4
:icons: font

This pipeline was developed for *single-end* fastq reads from ChIP-seq experiment. It is written with https://www.nextflow.io/[Nextflow framework]

This pipeline is based on different tools :

* https://www.bioinformatics.babraham.ac.uk/projects/fastqc/[FASTQC] for fastq analysis
* https://github.com/FelixKrueger/TrimGalore[Trim Galore] for trimming (by default)
* http://www.usadellab.org/cms/index.php?page=trimmomatic[Trimmomatic] for trimming (altenatively)
* http://bowtie-bio.sourceforge.net/bowtie2/index.shtml[Bowtie2] for alignment
* http://www.htslib.org/[samtools] for sorting, deduplicating and indexing bam files 
* https://github.com/shawnzhangyx/PePr[PePr] for narrow peak calling
* https://github.com/biocore-ntnu/epic2[epic2] for broad peak calling
* https://bedtools.readthedocs.io[bedtools] 
* facount and bedgraphtobigwig from http://hgdownload.soe.ucsc.edu/downloads.html#utilities_downloads[Kent utils] (http://hgdownload.soe.ucsc.edu/admin/exe/, https://github.com/ENCODE-DCC/kentUtils)
* http://bioinf.wehi.edu.au/featureCounts/[featureCounts] from subread package for count
* https://multiqc.info/[multiqc]
* https://www.r-project.org/[R] for merging peaks and many libraries: markdown, rmarkdown, optparse, magrittr, dplyr, stringr, tidyr, purrr, skimr, gplot2, scales, ggpubr, dt and the bioconductor libraries genomicranges and complexheatmap.

[WARNING]
====
This pipeline install all necessary software with conda. cf `environment.yml` file. +
Paired-end data are not supported in this pipeline! +
Docker is not available for this pipeline for the moment. +
====



## Install

https://docs.conda.io/projects/conda/en/latest/#[conda] (or https://docs.conda.io/en/latest/miniconda.html[miniconda]) and https://www.nextflow.io/[nextflow] must be installed. All the files of the pipeline must be copied to the directory where the pipeline should run. All the necessary programs (cf link:environment.yml[]) will be installed by conda  at the first launch of the pipeline. The `NXF_CONDA_CACHEDIR` environment variable can be used to specify an already existing conda environment.

## Running ChIP-seq_pipeline.nf

Print help of ChIP-seq_pipeline.nf :

  nextflow run ChIP-seq_pipeline.nf --help

The typical command for running the pipeline is as follows:

  nextflow run ChIP-seq_pipeline.nf --configFile design.csv --genome genome_file -profile slurm


 

The configuration file describe data used for analysis. A line consists of the following fields separated by commas : base name of the chipseq fastq file, base name of the input (control) fastq file, *sample* id and *condition* id. Example:

	C1_H3K4me3_subsample,C1_INPUT_subsample,C1,control
	T1_H3K4me3_subsample,T1_INPUT_subsample,T1,treatment
	C2_H3K4me3_subsample,C2_INPUT_subsample,C2,control
	T2_H3K4me3_subsample,T2_INPUT_subsample,T2,treatment

By defaults reads are searched in the directory specified by `--reads_dir` (by default: `./data/`) with the file extension specified by `--reads_ext` (by default: `.fastq.gz`) from the names provided in the config file. Ex: data/C1_H3K4me3_subsample.fastq.gz


----
    Usage:
    
    nextflow run ChIP-seq_pipeline.nf --genome 'genome.fasta' --outdir 'results' --configFile configFile.txt --reads_dir my_data --reads_ext .fq.gz  --sharp --index 'bowtie-index/*'
    
    
    Mandatory parameters
      --configFile Comma-separated file containing information about the samples in the experiment
      --genome Path to the fasta containing genome.
      --effectiveGenomeFraction and --readSize if no --sharp option. cf Peaks calling options 
    
    Input options
      --reads_dir: directory where the pipeline search reads. Default: <data>
      --reads_ext: extension file of the reads. Default: <.fastq.gz>
      --index: path to index genome files (surrounded by quotes). Default : deduced from --genome
    
     Trimming options: 
      --notrim: skip trimming step. (default : FALSE)
      --trim_galore: use trim-galore rather than trimmomatic. (default : FALSE)
      --trimmomatic_params: parameters for trimmomatic. Default: <HEADCROP:5 SLIDINGWINDOW:4:15 MINLEN:40>
      --trim_galore_stringency stringency of trim_galore. Default: <10>)
      
    Peak Calling options:
      --sharp: for narrow peak detection (default : FALSE)
      --windowSize: fixe a window size for PePr peak detection (default : automatically fixed by PePr)
      --chromSize: path to file describing genome pseudomolecules size (chromsize). 
          Allow to detect peaks only on listed pseudomolecules for broad peak calling.
      --effectiveGenomeFraction: value of the effective genome fraction. *Mandatory* for broad peak detection. 
      --readSize: length of reads. *Mandatory* for broad peak calling. 
    
    Peaks merging options:
      --label: string used as prefix for consensus peaks files and peaks names.
      --min_reps_consensus: minimum number of samples peaks to create a consensus peaks. 
          Default value: <0>
          If this parameter is set to 0 (default value), it's min_frac_reps_consensus that is used
      --min_frac_reps_consensus: same threshold but expressed as a fraction of the total number 
          of samples. Default value : <0.5>. 
          Default: --min_frac_reps_consensus 0.5. 
          Not used if --min_reps_consensus is defined with a value > 0
     
    Other options:
      --outdir: directory where results are stored. Default: <results> 
      --fastqc: make a FastQC analysis. Default: FALSE
      --help
----

If an incident happens, you could rerun your command line with `-resume` option.



## Pipeline operations


### Quality report, cleaning and mapping

Reads files are deduced from `--configFile` (and `--reads_dir`  `--reads_ext`). With the `--fastqc` option, a quality control of fastq files are performed by *fastqc*. Reads files are optionnaly cleaned by *trim-galore* or *trimmomatics* and mapped by *bowtie2* to the genome specified by the `--genome` option. Eventually before mapping, the pipeline create bowtie2 index for the genome. *samtools* is used for sort and deduplicate mapped reads.

Normalised bigWig files scaled to 1 million mapped reads are created from bam files with *BEDTools* and *bedGraphToBigWig*. Before that *faCount* was used to create chromsize file with all chromosome required by *bedGraphToBigWig*.

### Peaks calling, merging and counting

Without the `--sharp` option, *epic2* is used for peaks calling for each sample using each pair of chipseq/input bam files. This requires the input of the `--effective-genome-size` value and the `--readSize` value. The peak call is made only on the chromosomes listed in the mandatory `--chromSize` file.
All peaks files from the same condition are merged using *merge_peaks.Rmd*. 

With the `--sharp` options, *PePr* is used for peaks calling using all chipseq  and input files of the same condition.

After, epic2/merge or PePr, we obtain one peak file by condition. All of these files are merged into one peak file by *merge_peaks.Rmd* configured to create feature spanning all peaks.

This inter-condition consensus peaks file and the bam files precedently created are used to perform count (by *featureCount*). 

The last operation of the pipeline is the creation of a *multiqc* report from the outputs of the precedents steps.


#### merging peaks with merge_peaks.Rmd

*merge_peaks.Rmd* is a rmarkdown report for merging peaks and creating a report describing input peaks and their intersections.

All positions with an overlap of a minimum of `n peaks >= round*(min_frac_reps_consensus*nsample)` or `npeaks > min_reps_consensus` are kept and merged to create consensus peaks.


#### effective genome fraction

Epic2 require the value of effective genome fraction. The default value depends on the genome and
reads length, but is a number between 0 and 1. It's can be calculated with https://github.com/dib-lab/khmer[khmer]. For example, for a read size of 50bp

[source,sh]
----
GENOME=genome.fa
GSize=`grep -v '>' $GENOME|perl -pe 's/\n//g' | wc  -m`
unique-kmers.py $GENOME -k 50 2>| khmer.out
GEffectiveSize=`perl -0777 -pe '/mers:\s+(\d+)$/; $_=$1' khmer.out`
GEffectiveFraction=`perl -e "printf('%.5f', $GEffectiveSize/$GSize)"`
----


### Pipeline flowchart


[mermaid]
----
graph TD
C[/confFile/] --> R[/fastq/]
C --> S8_9
R --> |--fastqc| S1[fastqc]
G[/genome/] -.-> S3[bowtie2 index]
S3 --> S6[chromsize]
subgraph "Reads mapping"
R --> S2[trimming -> fastq]
S2 --> S4[mapping -> bam]
R --> |--notrim| S4
S3 --> S4
S4 --> S5[dedup -> bam]
end
S5 --> S7[bigwig]
S6 --> S7
subgraph "Peaks Calling, merging and counting"
S5 --> S8_9[/fileConf+bam dedup/]
S8_9 --> |--sharp| S8[PePr -> bed]
S8_9 --> |broad| S9[epic2 -> bed]
S9 --> |peaks by sample| S9b[merging by exp -> bed]
S9b --> S10[merging all peaks -> bed]
S8 --> S10
S10 --> S11[count -> tab]
S5 --> S11
end
----

