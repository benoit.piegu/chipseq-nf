#!/usr/bin/env nextflow
// -*- mode: java -*-

/*
#############################################################
### ChIP-seq pipeline : Trimming - Mapping - Peak calling ###
#############################################################
Script started in January 2019
Authors :
    Coralie Gimonnet <coralie.gimonnet@inra.fr>
    Benoît Piégu <benoit.piegu@inra.fr>
This pipeline is adaptated for single-end ChIP-seq data.
*/

/* Default parameters */

params.genome = "genome.fasta"
params.reads_dir = "data"
params.reads_ext = ".fastq.gz"
// params.outdir = "results" // configured in nextflow.config
params.configFile = "configFile.txt"
params.label = ""

/* Optional parameters initialization */
params.notrim = false
//params.trimmomatic = true
params.trim_galore = false
params.help = false
params.debug = false
params.fastqc = false
params.trimmomatic_params = "HEADCROP:5 SLIDINGWINDOW:4:15 MINLEN:40"
params.trim_galore_stringency = 10
params.windowSize = false //PePr
params.index = false
params.sharp = false
params.chromSize = false
params.readSize = false
params.effectiveGenomeFraction = false
params.min_reps_consensus = 0
params.min_frac_reps_consensus = 0.5

params.cpus_pepr = 20

genome = file(params.genome)

configFile = file(params.configFile)
merge_report = file('share/merge_peaks.Rmd')
config_multiqc = file('share//multiqc_config.yaml')
label = params.label == "" ? "" : "${params.label}_"


if (params.help){
    log.error """
  Usage:
 
  nextflow run ChIP-seq_pipeline.nf --genome genome.fasta --outdir results --configFile configFile.txt --reads_dir my_data --reads_ext .fq.gz  --sharp --index 'bowtie-index/*'
 
 
  Mandatory parameters
    --configFile Comma-separated file containing information about the samples in the experiment
        Default: <configFile.txt>
    --genome Path to the fasta containing genome. Default: <genome.fasta>
    --effectiveGenomeFraction and --readSize if no --sharp option. cf Peaks calling options 

  Input options
    --reads_dir: directory where the pipeline search reads. Default: <data>
    --reads_ext: extension file of the reads. Default: <.fastq.gz>
    --index: path to index genome files (surrounded by quotes). Default : deduced from --genome
 
   Trimming options: 
    --notrim: skip trimming step. (default : FALSE)
    --trim_galore: use trim-galore rather than trimmomatic. (default : FALSE)
    --trimmomatic_params: parameters for trimmomatic. Default: <HEADCROP:5 SLIDINGWINDOW:4:15 MINLEN:40>
    --trim_galore_stringency stringency of trim_galore. Default: <10>)
    
  Peaks calling options:
    --sharp: for narrow peak detection (default : FALSE)
    --windowSize: fixe a window size for PePr peak detection (default : automatically fixed by PePr)
    --chromSize: path to file describing genome pseudomolecules size (chromsize). 
        Allow to detect peaks only on listed pseudomolecules for broad peak calling.
    --effectiveGenomeFraction: value of the effective genome fraction. *Mandatory* for broad peak detection. 
    --readSize: length of reads. *Mandatory* for broad peak calling. 
 
  Peaks merging options:
    --label: string used as prefix for consensus peaks files and peaks names.
    --min_reps_consensus: minimum number of samples peaks to create a consensus peaks. 
        Default value: <0>
        If this parameter is set to 0 (default value), it's min_frac_reps_consensus that is used
    --min_frac_reps_consensus: same threshold but expressed as a fraction of the total number 
        of samples. Default value : <0.5>. 
        Default: --min_frac_reps_consensus 0.5. 
        Not used if --min_reps_consensus is defined with a value > 0
 
  Other options:
    --outdir: directory where results are stored. Default: <results> 
    --fastqc: make a FastQC analysis. Default: FALSE
    --help
    """
    exit 0
}

log.error """\
            ChIP-seq PIPELINE
        ================================================================================ 
    Paired-end data are not supported in this pipeline!
            configFile = <${params.configFile}>
            genome = <${params.genome}>
        Inputs parameters
            reads_dir = <${params.reads_dir}>
            reads_ext = <${params.reads_ext}>
            index = <${params.index}>
        Trimming options
            notrim = <${params.notrim}>
            trim_galore = <${params.trim_galore}>
            trimmomatic_params = <${params.trimmomatic_params}>
            trim_galore_stringency = <${params.trim_galore_stringency}>
        Peak calling options
            sharp = <${params.sharp}>
            windowSize = <${params.windowSize}>
            chromSize = <${params.chromSize}>
            effectiveGenomeFraction = <${params.effectiveGenomeFraction}>
            readSize  = <${params.readSize}>
        Peak Merging options
            label  = <${params.label}>
            min_reps_consensus = <${params.min_reps_consensus}>
            min_frac_reps_consensus = <${params.min_frac_reps_consensus}>
        Other options
            outdir = <${params.outdir}>
            fastqc = <${params.fastqc}>
        """
        .stripIndent()

def to_freads(fname) {
    file(params.reads_dir + "/" + fname + params.reads_ext)
}

def make_pepr_control(exp_list, cpus, windowSize){
    def exp = exp_list[0]
    def files_lol = exp_list[1]
    //def chip_list = files_lol.each { it -> it[0]}
    //def input_list = files_lol.each { it -> it[0]}
    def str = "#filetype\tfilename\n"
    files_lol.each { it ->
        str += "chip1\t" + it[0].getName() + "\n"
        str += "input1\t" + it[1].getName() + "\n"
    }
    str += "file-format\tbam\n"
    str += "peaktype\tsharp\n"
    str += "difftest\tfalse\n"
    str += "threshold\t1e-5\n"
    str += "name\t" + exp + "\n"
    str += "num-processors\t" + cpus + "\n"
}

// --chromSize is mandatory for the optionnal epic detection and always for creation of bigwig files
chromsize_file = file(params.chromSize).toAbsolutePath()
chromSize = Channel
    .fromPath(params.chromSize)
    .dump(tag: "chromSize")
    .ifEmpty { exit 1, "Chromosome size file not found : ${params.chromSize}" }

/*
* Create a channel for peak detection file
* which lists all files needed for peak calling
*/

iline = 0
n_missing_file = 0

iiline = 0
ianalysis_exp = 1
iexp = 2
itype = 3
ifile = 4

Channel
    .from (configFile.readLines())
    .ifEmpty { exit 1, "Missing config file for peak calling. Specify with --configFile"}
    // a.1,a.2,p,A
    .map { line ->
        def list = line.split(',')
        def chip_sample_id = list[0]
        def input_sample_id = list[1]
        def analysis_id = list[2]
        def exp = list[3]
           iline++
           println("# ${iline} ${chip_sample_id} ${input_sample_id} ${exp}")
           [[iline, analysis_id, exp, 'chip', chip_sample_id ],
           [iline, analysis_id, exp, 'input', input_sample_id]]
    }
    // [[1, 'p', 'A', 'chip', a.1.fastq.gz], [1, 'p', 'A', 'input', a.2.fastq.gz]]
    .flatMap()
    // [1, 'p', 'A', 'chip', a.1.fastq.gz],
    // [1, 'p', 'A', 'input', a.2.fastq.gz]]
    .map { it ->
           it[ifile] = to_freads(it[ifile]);
           if (! it[ifile].exists() ){
               println("#[ERR] file  <${it[ifile]}> not exist! (line ${it[iiline]} in conf file)")
               n_missing_file++
           }
           it
    }
    // [1, 'p', 'A', 'chip', /data/a.1.fastq.gz]
    // [1, 'p', 'A', 'input', /data/a.2.fastq.gz]
    .dump(tag: "reads_files_trimming")
    .into { reads_files_trimming; input_reads_fastqc; input_reads_pepr}

if (n_missing_file) exit 1


input_reads_fastqc
    .map { it[ifile] }
    .dump(tag: "reads_fastqc")
    .set { reads_fastqc }

/*
* Step 1. FASTQC Analysis
*/

if (params.fastqc) {
    process fastqc {
        publishDir "${params.outdir}/fastqc", mode: 'copy',
            saveAs: {filename -> filename.indexOf(".zip") > 0 ? "zips/$filename" : "$filename"}
        input:
            file reads from reads_files_fastqc
        output:
            file '*_fastqc.{zip,html}' into fastqc_results
        script:
            """
            fastqc -q $reads
            """
    }
} else {
    fastqc_results = Channel.from(false)
}


/*
* Step 2. Trimming
*/

if (params.notrim) {
    trimmed_reads = reads_files_trimming
    trimgalore_results = Channel.from(false)
} else {
    if (!params.trim_galore){
        process trimmomatic{
            cpus 2
            publishDir "${params.outdir}/trimmotatic",mode : 'symlink',
                saveAs: {filename ->
                    if (filename.indexOf("trimmomatic.log") > 0 || filename.indexOf("trimmomatic.stat") > 0) "logs/$filename"
                }
            input:
                tuple iline, analysis_id, exp, type, reads from reads_files_trimming
            output:
                tuple val(iline), val(analysis_id), val(exp), val(type), file("${reads.simpleName}_trimmed.fastq.gz") into trimmed_reads
                tuple file("${reads.simpleName}.trimmomatic.log"),
                    file("${reads.simpleName}.trimmomatic.stat"),
                    file("${reads.simpleName}.trimmomatic.tab") into trimmomatic_results
            script:
            //${reads.simpleName}
                """
                export LANG=C
                trimmomatic SE -threads ${task.cpus} $reads ${reads.simpleName}_trimmed.fastq.gz ${params.trimmomatic_params} \\
                -trimlog ${reads.simpleName}.trimmomatic.tab -summary ${reads.simpleName}.trimmomatic.stat \\
                 2> ${reads.simpleName}.trimmomatic.log
                """
        }
    } else {
        process trimm_galore {
        cpus 8
        publishDir "${params.outdir}/trim_galore",mode : 'copy',
            saveAs: {filename ->
                if (filename.indexOf("trimming_report.txt") > 0) "logs/$filename"
            }
        input:
            tuple iline, analysis_id, exp, type, reads from reads_files_trimming
        output:
            tuple val(iline), val(analysis_id), val(exp), val(type), file('*.fq.gz') into trimmed_reads
            file "*trimming_report.txt" into trimgalore_results
        script:
            """
            trim_galore --cores ${task.cpus} --gzip --stringency ${params.stringency} $reads
            """
        }
    }
}


/*
* Step 3. Build genome index with Bowtie2
*/
if (params.index) {
    //bowtie_index = file("${params.index}.fa")
    bowtie_index = Channel
        .fromPath("${params.index}.*.bt2")
        .ifEmpty { exit 1, "Bowtie2 index not found : ${params.index}" }
} else {
    process buildIndex {
        cpus 8
        tag "${genome.baseName}"
        publishDir "${params.outdir}/genome", mode : 'symlink'
        input:
            file genome from genome
        output:
            file "${genome.baseName}.*" into bowtie_index
        script:
            """
            bowtie2-build ${genome} ${genome.baseName} --threads ${task.cpus}
            """
    }
}


/*
* Step 4. Mapping with Bowtie2
*/

process mapping {
    cpus 18
    memory_per_thread = params.max_memory_per_thread.intdiv(2)
    memory = "${memory_per_thread * cpus} GB"
    tag "${genome.baseName}"
    publishDir "${params.outdir}/mapping", mode : 'symlink',
        saveAs: {filename ->
            if (filename.indexOf(".txt") > 0  || filename.indexOf(".log") > 0) "logs/$filename"
            else if (filename.indexOf(".bam") > 0) "bam/$filename"
        }
    input:
        file index from bowtie_index.collect()
        tuple iline, analysis_id, exp, type, reads from reads_files_trimming from trimmed_reads
    output:
        tuple file("${reads.simpleName}_flagstat.txt"), file("${reads.simpleName}.bwt2.log") into logs_aln
        tuple val(iline), val(analysis_id), val(exp), val(type), file("${reads.simpleName}.bwt2.bam") into bam_aln
    script:
        """
        bowtie2 --end-to-end -p ${task.cpus} --very-sensitive -U $reads -x ${genome.baseName} \\
         2> ${reads.simpleName}.bwt2.log \\
         | samtools view -uS -@4 - | samtools sort -l9 -@${task.cpus} -O bam  - >| ${reads.simpleName}.bwt2.bam
        samtools flagstat ${reads.simpleName}.bwt2.bam > ${reads.simpleName}_flagstat.txt
        """
}

/*
* Step 5. Deduplication
*/

process deduplicate {
    tag "${bam.simpleName}"
    cpus 8
    publishDir "${params.outdir}/dedup", mode : 'symlink',
        saveAs: {filename ->
            if (filename.indexOf(".txt") > 0 || filename.indexOf(".log") > 0) "logs/$filename"
            else if (filename.indexOf(".bam") > 0 || filename.indexOf(".bai") > 0) "bam/$filename"
        }
    input :
        tuple iline, analysis_id, exp, type, file(bam) from bam_aln
    output:
        tuple val(iline), val(analysis_id), val(exp), val(type), file("${bam.simpleName}.dedup.bam") into (input_bam, input_pepr_bam, bam_dbg)
        tuple file("${bam.simpleName}.dedup.bam"), file("${bam.simpleName}_flagstat.dedup.txt") into input_bigwig
        tuple file("${bam.simpleName}_flagstat.dedup.txt"), file("${bam.simpleName}.dedup.log") into logs_dedup
        file("${bam.simpleName}.dedup.bam") into input_bam4count
    script:
        """
        samtools rmdup -s $bam ${bam.simpleName}.dedup.bam 2> ${bam.simpleName}.dedup.log
        samtools index ${bam.simpleName}.dedup.bam
        samtools flagstat ${bam.simpleName}.dedup.bam > ${bam.simpleName}_flagstat.dedup.txt
        """
}

bam_dbg.dump(tag: "bam_dbg")

/*
* Step 6. Create chrom.size file for the creation of the bigwig files
*/

process chromsizes {
    tag "${params.genome}"
    cpus 1
    publishDir "${params.outdir}/genome", mode : 'symlink'
    input:
            file genome from genome
    output:
        file "${genome.baseName}.chrom.size" into chromsize
    script:
        """
        faCount ${genome} | \\
          perl -F'\\t' -ne 'next if /^(#|total)/; print "\$F[0]\\t\$F[1]\\n"' >| ${genome.baseName}.chrom.size
        """
}


/*
* Step 7. Bigwig
*/
process bigwig {
    tag "${bam.simpleName}"
    cpus 1
    publishDir "${params.outdir}/bigwig", mode : 'symlink'
    input:
        tuple file(bam), file(flagstat) from input_bigwig
        file(chromsize) from chromsize
    output:
        file("${bam.simpleName}.bigwig") into bigwig
        path '*scale_factor.txt'
    script:
        """
        # scaling to 1 million mapped reads
        SCALE_FACTOR=`grep 'mapped (' $flagstat | awk '{print 1000000/\$1}'`
        echo \$SCALE_FACTOR > ${bam.simpleName}.scale_factor.txt
        genomeCoverageBed -ibam $bam -bg -scale \$SCALE_FACTOR  | sortBed -i - > ${bam.simpleName}.bedGraph &&
        bedGraphToBigWig ${bam.simpleName}.bedGraph $chromsize ${bam.simpleName}.bigwig
        """
}

input_bam
    // [iline, analysis_exp, exp, type, file]
    // [1, 'p', 'A', 'chip', /data/a.1.bam]
    // [1, 'p', 'A', 'input', /data/a.2.bam]
    .groupTuple(by: iiline)
    // [1, ['p', 'p'], ['A', 'A'], ['chip', 'input'], [/data/a.1.bam, /data/a.2.bam]]
    .map {
        // cf https://github.com/nextflow-io/nextflow/issues/347
        ichip = 0
        iinput = 1
        if (it[itype][0] == 'input') {
            ichip = 1
            iinput = 0
        }
        [it[iiline], it[ianalysis_exp][0], it[iexp][0], 'NA', it[ifile][ichip], it[ifile][iinput]] }
    // [iline, analysis_exp, exp, type, chip_file, input_file]
    // [1, 'p', 'A', 'NA', /data/a.1.bam, /data/a.2.bam]
    .dump(tag: "rearranged_bam")
    .into { input_pepr_control; input_epic_bysample; input_epic_byexp }

/*
* Step 8. Peak detection with PePr (sharp peaks)
*/
if (params.sharp) {
    input_pepr_bam
        .map{ it -> [ it[iexp], it[ifile] ] }
        .groupTuple(by: 0)
        // gather exp file by exp
        // [exp0, [[chipfile00, inputfile00, chipfile01, inputfile01, ..], [exp1, ...]]
        .dump(tag: "pepr_bam")
        .set { pepr_bam}
    // -> by  exp, list of bam

    // create content of control files for PePr
    input_pepr_control
        .map{ it -> [ it[iexp], [it[ifile], it[ifile+1]] ] }
        .groupTuple(by: 0)
        // gather exp file by exp
        // [exp0, [[chipfile00, inputfile00], [chipfile0, inputfile01]]]
        // [exp1, [[chipfile10, inputfile10], ...]
        .map { it -> [ it[0], make_pepr_control(it, params.cpus_pepr, params.windowSize) ] }
        .dump(tag: "input_pepr_control")
        .set { input_pepr_control }
    // -> by each exp, pepr_control file and list of bam by pairs chip/input

    // create control file for PePr
    /*
    * Step 8a. Create config file for PePr
    */
    process configFilePePr {
        executor 'local'
        publishDir "${params.outdir}/peak_calling_pepr", mode : 'copy'
        input:
            tuple exp, control from input_pepr_control
        output:
            tuple val(exp), file("${exp}.setting_PePr.txt") into pepr_control
        script:
            """
            echo '$control' >| ${exp}.setting_PePr.txt
            """
    }

    // join by exp control files (1) and bam files (n)
    pepr_control
        .join(pepr_bam)
        .dump(tag: "input_pepr")
        .set { input_pepr }

    /*
    * Step 8b. Peak detection
    */
    process PePr {
        cpus params.cpus_pepr
        tag "${control}"
        publishDir "${params.outdir}/peak_calling_pepr", mode : 'copy'
        input:
            tuple exp, file(control), file(bam) from input_pepr
        output:
            file "*bed" optional true into narrowPeakFiles, narrowPeakID
            file "${exp}__PePr_parameters.txt" into post_pepr_parameters
            file "*.log" into logs_pepr
            set val('pepr'), val(exp), file("*bed") into input_merge_exp
        script:
            """
            PePr -p $control
            """
    }
} else {
/*
* Step 9. Peak detection with epic2 (broad peaks)
*/

    if (!params.readSize) {
        println "Please specify a read size parameter for peak detection"
        System.exit(1)
    }
    if (!params.effectiveGenomeFraction) {
        println "Please specify a effective genome fraction parameter for broad peaks detection"
        System.exit(1)
    }

    /*
    * Step 9a. Peak detection with epic2 by sample
    */
    process epicDetection_bySample {
        cpus 1
        tag "epic_${chip_bam[0].simpleName}"
        publishDir "${params.outdir}/peak_calling_epic_bysample", mode : 'copy'
        input:
            tuple val(iline), val(analysis_id), val(exp), val(type), file(chip_bam), file(input_bam) from input_epic_bysample
            // file(chrsize) from chromSize
        output:
            file "*.bed" into broadPeakFiles_bysample, broadPeakID_bysample
            tuple val(exp), val (analysis_id), path("*.bed") into input_merge_epic_peaks_bysample
        script:
                """
                echo "epic2"
                epic2 \\
                --chromsizes ${chromsize_file} \\
                --control ${input_bam} \\
                --treatment ${chip_bam} \\
                --fragment-size ${params.readSize} \\
                --gaps-allowed 2 \\
                --false-discovery-rate 0.05 \\
                --effective-genome-fraction ${params.effectiveGenomeFraction} \\
                --output ${chip_bam.simpleName}.bed > ${chip_bam.simpleName}.txt 2> ${chip_bam.simpleName}.log
                """
    }

     input_merge_epic_peaks_bysample
        // gather names and files by exp and adjust min_reps_consensus
        .groupTuple(by: 0)
        // set nmin_reps
        .map { it->
            n = it[1].size()
            min_nreps = params.min_reps_consensus
            if (min_nreps > n) min_nreps = n
            if (min_nreps == 0) {
                min_nreps = (params.min_frac_reps_consensus*n).setScale(0, BigDecimal.ROUND_HALF_UP)
            }
            it + min_nreps
        }
        .dump(tag: "input_merge_peaks_bysample")
        .set { input_merge_epic_peaks_bysample}

    /*
    * Step 9b. Merging samples peaks by exp
    */
    process mergeEpicPeaks_BySample{
        cpus 1
        tag "merge_peaks_${exp}"
        publishDir "${params.outdir}/peak_calling_epic_bysample/merged", mode : 'copy'
        input:
            tuple val(exp), val(analysis_id), file(bed_files), val(min_reps) from input_merge_epic_peaks_bysample
        output:
            out_prefix = "${label}${exp}_n${min_nreps}"
            path "${task.out_prefix}.html"
            path "${task.out_prefix}.saf"
            path "${task.out_prefix}.bed"
            tuple val("epic_bysample"), val(exp), path("${task.out_prefix}.bed") into input_merge_exp
        script:
            lbed = "c(" + bed_files.collect{ "'${it}'"}.join(", ") + ")"
            lid = "c(" + analysis_id.collect{ "'${it}'"}.join(", ") + ")"
            title = (params.label== "" ? "" : "${params.label} ") + "Merge ${exp} peaks"
            """
            cp ${merge_report} .
            R --vanilla -e "rmarkdown::render('${merge_report.getName()}', \\
            output_file='${task.out_prefix}.html', \\
            clean=T, \\
            params=list(echo=F, debug=F, \\
            title='${title}', filetype='epic2', peaks_files=${lbed}, sample_names=${lid}, \\
            chrom_size='${chromsize_file}', \\
            saf_file='${task.out_prefix}.saf', bed_file='${task.out_prefix}.bed', \\
            nmin_sample=${min_nreps}, peak_label_prefix='peak_${exp}'))"
            """
    }

    input_epic_byexp
        .map{ it -> [ it[iexp], it[ifile], it[ifile+1]] }
        .groupTuple(by: 0)
        .dump(tag: "intput_epic_byexp")
        .set { input_epic_byexp }

    /*
    * Step 9c. Peak detection with epic2 by exp
    */
    process epicDetection_byExp {
        cpus 1
        tag "epic_${exp}"
        publishDir "${params.outdir}/peak_calling_epic_byexp", mode : 'copy'
        input:
            tuple val(exp), file(chip_bam), file(input_bam) from input_epic_byexp
            // file(chrsize) from chromSize
        output:
            file "*.bed" into broadPeakFiles_byexp, broadPeakID_byexp
        script:
                """
                echo "epic2"
                epic2 \\
                --chromsizes ${chromsize_file} \\
                --control ${input_bam} \\
                --treatment ${chip_bam} \\
                --fragment-size ${params.readSize} \\
                --gaps-allowed 2 \\
                --false-discovery-rate 0.05 \\
                --effective-genome-fraction ${params.effectiveGenomeFraction} \\
                --output ${exp}.bed > ${exp}.txt 2> ${exp}.log
                """
    }
}

/*
* Step 10. merge peaks from all exp
*/

input_merge_exp
    .groupTuple(by: 0)
    .dump(tag: "input_merge_exp")
    .set{input_merge_exp}

process mergePeaks_allExp {
    cpus 1
    tag "merge_peaks_exp"
    publishDir "${params.outdir}/peak_calling_${peaksType}/exp_merged", mode : 'copy'
    input: tuple val(peaksType), val(exp_names), path(bed_files) from input_merge_exp
    output:
        out_prefix = "${label}all"
        path "${task.out_prefix}.html"
        path "${task.out_prefix}.saf"
        path "${task.out_prefix}.bed"
        tuple val(peaksType), path("${task.out_prefix}.saf") into input_count
    script:
        lbed = "c(" + bed_files.collect{ "'${it}'"}.join(", ") + ")"
        lnames = "c(" + exp_names.collect{ "'${it}'"}.join(", ") + ")"
        title = (params.label== "" ? "" : "${params.label} ") + "Merge peaks"
        """
        cp ${merge_report} .
        R --vanilla -e "rmarkdown::render('${merge_report.getName()}', \\
        output_file='${task.out_prefix}.html', \\
        clean=T, \\
        params=list(echo=F, debug=F, \\
        title='${title}', filetype='bed', peaks_files=${lbed}, sample_names=${lnames}, \\
        chrom_size='${chromsize_file}', \\
        saf_file='${task.out_prefix}.saf', bed_file='${task.out_prefix}.bed', \\
        nmin_sample=1))"
        """
}


/*
* Step 11. count
*/

process count{
    cpus 8
    tag "count"
    publishDir "${params.outdir}/peak_calling_${peaksType}/exp_merged", mod : 'copy'
    input:
        tuple val(peaksType), path(fsaf) from input_count
        file(input_bam) from input_bam4count.collect()
    output:
        file "${label}all.counts"
        file "${label}all.counts.summary" into logs_count
    script:
        """
        featureCounts -T ${task.cpus} -a  ${fsaf} -o ${label}all.counts -F SAF ${input_bam}
        # clean _trimmed.dedup.bam extension in count file:
        perl -pi~ -e 's/_trimmed.dedup.bam(?!")//g' ${label}all.counts
        """
}

/*
* Step 12. multiQC report
*/

process multiQC {
    publishDir "${params.outdir}/", mode : 'copy'
    input:
        path ('trimmomatic/*') from trimmomatic_results.collect().ifEmpty([])
        path ('mapping/*') from logs_aln.collect()
        path ('dedup/*') from logs_dedup.collect()
        path ('count/*') from logs_count.collect().ifEmpty([])
    output: path "${label}multiqc_report.html"
    script:
        title = (params.label== "" ? "" : "[${params.label}] ") + "MultiQC Report"
        """
        multiqc .  --config ${config_multiqc} --filename ${label}multiqc_report.html --title "${title}"
        """
}

/*
* Step 13. Software version
*/

process softwareVersion {
    publishDir "${params.outdir}/version", mode : 'copy'
    output:
        file 'software_version.txt' into software_version
    script:
        """
        echo '# FastQC version:' > software_version.txt
        fastqc --version >> software_version.txt || echo '# fastqc error' >> software_version.txt
        echo '# Trim Galore! version:' >> software_version.txt
        trim_galore --version >> software_version.txt || echo '# trim_galore error' >> software_version.txt
        echo '# Cutadapt version:' >> software_version.txt
        cutadapt --version >> software_version.txt || echo '# cutadapt error' >> software_version.txt
        echo '# bowtie2 version:' >> software_version.txt
        bowtie2 --version >> software_version.txt || echo '# bowtie2 error' >> software_version.txt
        echo '# epic version:' >> software_version.txt
        epic2 --version >> software_version.txt || echo '# epic2 error' >> software_version.txt
        echo '# PePr version:' >> software_version.txt
        PePr --version >> software_version.txt || echo '# PePr error' >> software_version.txt
        echo '# Samtools version:' >> software_version.txt
        samtools --version >> software_version.txt || echo '# samtools error' >> software_version.txt
        """
}
